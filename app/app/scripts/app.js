'use strict';

/**
 * @ngdoc overview
 * @name sirianosApp
 * @description
 * # sirianosApp
 *
 * Main module of the application.
 */
 angular
 .module('sirianosApp', [
 	'ngAnimate',
 	'ngCookies',
 	'ngResource',
 	'ngRoute',
 	'ngSanitize',
 	'ngTouch',
 	'uiGmapgoogle-maps',
 	'oauth.io',
 	'ui-rangeSlider',
 	'ngAutocomplete',
 	'hashtagify'
 	]).config(
 	['uiGmapGoogleMapApiProvider', function(GoogleMapApiProviders) {
 		GoogleMapApiProviders.configure({
 			china: true
 		});
 	}]
 	).config(function ($routeProvider) {
 		$routeProvider
 		.when('/', {
 			templateUrl: 'views/main.html',
 			controller: 'MainCtrl',
 			controllerAs: 'main'
 		})
 		.otherwise({
 			redirectTo: '/'
 		});
 	}).filter('tdate', function() {
	  return function(tdate) {
	  	var system_date = new Date(Date.parse(tdate));
	  	var user_date = new Date();
	  	var diff = Math.floor((user_date - system_date) / 1000);
	  	if (diff <= 1) {return "μόλις τώρα";}
	  	if (diff < 20) {return "πριν από " + diff + " δευτερόλεπτα";}
	  	if (diff < 40) {return "μισό λεπτό πριν";}
	  	if (diff < 60) {return "λιγότερο από ένα λεπτό πριν";}
	  	if (diff <= 90) {return "ένα λεπτό πριν";}
	  	if (diff <= 3540) {return "πριν από " + Math.round(diff / 60) + " λεπτά";}
	  	if (diff <= 5400) {return "1 ώρα πριν";}
	  	if (diff <= 86400) {return "πριν από " + Math.round(diff / 3600) + " ώρες";}
	  	if (diff <= 129600) {return "1 μέρα πρίν";}
	  	if (diff < 604800) {return "πριν από " + Math.round(diff / 86400) + " μέρες";}
	  	if (diff <= 777600) {return "1 εβδομάδα πριν";}
	  	return "στις " + system_date;
	  }
});
