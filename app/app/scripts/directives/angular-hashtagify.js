angular.module('hashtagify', []);

angular.module('hashtagify')
.directive('hashtagify', ['$timeout', '$compile',
    function($timeout, $compile) {
        return {
            restrict: 'A',
            scope: {
                hashtag : '@hashtag'
            },
            link: function(scope, element, attrs) {
                $timeout(function() {
                    var html = element.html();

                    if (html === '') {
                        return false;
                    }

                    html = html.replace(/(|\s)*@(\w+)/g, '$1<a target="_blank" href="https://www.twitter.com/$2" class="hashtag">@$2</a>'); 
                
                    html = html.replace(/(^|\s)*#(\w+)/g, '$1<a target="_blank" href="https://www.twitter.com/hashtag/$2?src=hash" class="hashtag">#$2</a>');

                    if (scope.hashtag) {
                        html = html.replace(new RegExp("(^|\s)*#" + scope.hashtag, "i"), "<span class=\"red\">$&</span>");
                    }

                    element.html(html);
                    
                    $compile(element.contents())(scope);
                }, 0);
            }
        };
    }
]);
