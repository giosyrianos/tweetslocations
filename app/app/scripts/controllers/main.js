'use strict';

angular.module('sirianosApp')
  .controller('MainCtrl', function ($scope, $http, $timeout) {
    $scope.trustHtml = function(html) {
            // Sanitize manually if necessary. It's likely this
            // html has already been sanitized server side
            // before it went into your database.
            // Don't hold me liable for XSS... never assume :~)
            return $sce.trustAsHtml(html);
        };
    $scope.min = 1;
    $scope.tweets = [];
    $scope.hashtag = '';
    $scope.autocomplete = 'Athina, Κεντρικός Τομέας Αθηνών, Ελλάδα';
    $scope.map = { center: { latitude: 37.95286092, longitude: 23.74282837 }, zoom: 8};
    $scope.options = {scrollwheel: true};
    $scope.showModal = false;
    $scope.tweetType = 'recent';
    $scope.firstRun = true;
    $scope.selectedTweet = {};
    $scope.windowOptions = {
      zIndex:9999999
    };
    $scope.map.circle = {
      id: 1,
      center: {
          latitude: 37.95286092,
          longitude: 23.74282837
      },
      radius: 30000,
      stroke: {
          color: '#08B21F',
          weight: 2,
          opacity: 1
      },
      fill: {
          color: '#08B21F',
          opacity: 0.5
      },
      geodesic: true,
      draggable: true,
      clickable: true,
      editable: false,
      visible: true,
      control: {},
      events: {
        dragend : function(){
          $scope.getTweets($scope.map.circle.center.latitude, $scope.map.circle.center.longitude, $scope.map.circle.radius, $scope.tweetType, $scope.hashtag);
        }
      }
    };

    $scope.getTweets = function(lat, long, radius, type, hashtag){
      angular.copy({}, $scope.selectedTweet);
      $scope.firstRun = false;
      $scope.showModal = true;
      $scope.tweets = [];
      var data = {
        lat: lat,
        long: long,
        radius: $scope.map.circle.radius / 1000,
        type: type,
        hashtag: hashtag
      };
      $http.post('http://' + window.location.hostname + ':3000/api/tweets', data).then(function(response){
        $scope.showModal = false;
        $scope.tweets = response.data;
        $scope.markerTweets = [];

        for(var i = 0; i < $scope.tweets.length; i++){
          if($scope.tweets[i].coordinates){
            $scope.tweets[i].gmapOptions = {
              title : $scope.tweets[i].text
            };
            $scope.markerTweets.push($scope.tweets[i]);
          }
        }
      });
    };

    $scope.setSelectedTweet = function(marker,event,tweet){
      $timeout(function(){
        $scope.selectedTweet = tweet;
      },1);
    };

    $scope.prepareTweetSearch = function(value){
        var lat = value.geometry.location.lat();
        var long = value.geometry.location.lng();
        $scope.map.circle.center.latitude = lat;
        $scope.map.circle.center.longitude = long;
        $scope.map.center.latitude = lat;
        $scope.map.center.longitude = long;
        $scope.getTweets($scope.map.circle.center.latitude, $scope.map.circle.center.longitude, $scope.map.circle.radius, $scope.tweetType, $scope.hashtag);
    };

    $scope.$watch('tweetType', function(value) {
      if(value && $scope.autocompleteDetails){
        $scope.prepareTweetSearch($scope.autocompleteDetails);
      }
    });

    $scope.$watch('autocompleteDetails', function(value) {
      if(value){
        $scope.prepareTweetSearch(value);
      }
    });
});
