var bodyParser = require('body-parser');
var Twitter = require('twitter');

var client = new Twitter({
  consumer_key: '5GbEztQQrGpPup9UbaAZjQp51',
  consumer_secret: 'a67URXoVk3aIlgV9ZcrGlz0mjwmsColByCZQXP07P4HKhMG4Bh',
  access_token_key: '388953075-RaYpbi70iqWb6zStvxsBjHcMwgBHagKHdMT0CS08',
  access_token_secret: '31zPe08tyaxuoz4hRH5Wwm01EAelxDyMZFmIQuRR9LGCv'
});

var express = require('express');
var app = express();

var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}

app.use(bodyParser.json());
app.use(allowCrossDomain);

app.post('/api/tweets', function(req, res) {
    var lat = req.body.lat;
    var long = req.body.long;
    var radius = req.body.radius;
    var type = req.body.type;
    var hashtag = req.body.hashtag;

    getTweets(lat, long, radius, type, hashtag, function(tweets){
      res.setHeader('Content-Type', 'application/json');
      res.send(JSON.stringify(tweets));
    });
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

function getTweets(lat, long, radius, type, hashtag, callback){
  var queryString = (hashtag!=null ? ('#' + hashtag): '');
  var params = {'q': queryString, 'count':100, 'geocode':lat + ',' + long + ',' + radius + 'km', 'result_type': type};
  client.get('search/tweets.json', params, function(error, tweets, response) {
    if(tweets){
      callback(tweets.statuses);
    }else{
      callback([]);
    }
  });
}