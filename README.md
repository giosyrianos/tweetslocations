# Welcome to TweetLocations!

Discover tweets coming from the region of your choice.

## Use

Define the area you prefer on the map, or in the search bar and hit "Search".

## Installation

### You will need:

- Node.JS
- Grunt.JS

### Step1:

Access node_app folder from your Command Line and execute "node app".

### Step2:

In a new terminal window access app folder and execute "grunt serve".
>App available @ http://localhost:9000